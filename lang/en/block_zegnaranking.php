<?php
// This file is part of Ranking block for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Zegna ranking block english language translation
 *
 * @package    block_zegnaranking
 * @copyright  2017 Willian Mano http://conecti.me
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Zegna Ranking Block';
$string['ranking'] = 'Zegna ranking';
$string['ranking:addinstance'] = 'Add a new zegna ranking block';
$string['nostudents'] = 'No students to show';
$string['nomodules'] = 'No modules to show';
$string['nogroups'] = 'No groups to show';

$string['configuration'] = 'Zegna block configuration';
$string['blocktitle'] = 'Block title';
$string['chooseagroup'] = 'Choose a group';

$string['northamerica'] = 'North america';
$string['latam'] = 'Latam';
$string['europe'] = 'Europe';
$string['gcr'] = 'Gcr';
$string['asia'] = 'Asia pacific';
$string['supply_chain'] = 'Supply chain';

$string['region_northamerica'] = 'North america group';
$string['region_latam'] = 'Latam group';
$string['region_europe'] = 'Europe group';
$string['region_gcr'] = 'Gcr group';
$string['region_asia'] = 'Asia pacific group';
$string['region_supply_chain'] = 'Supply chain group';

$string['module'] = 'MODULE';
$string['pts'] = 'PTS';
$string['table_position'] = 'Pos';
$string['table_points'] = 'Points';

$string['map'] = 'Map';
$string['mapranking'] = 'General Ranking View';
$string['group'] = 'Group';
$string['groupranking'] = 'Group Ranking View';
$string['backtomap'] = 'Back to the map';
$string['module'] = 'Module';
$string['moduleranking'] = 'Module Ranking View';
$string['individualclassification'] = 'Individual classification';
