<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Zegna ranking local lib.
 *
 * @package   block_zegnaranking
 * @copyright 2017 Willian Mano http://conecti.me
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_zegnaranking;

defined('MOODLE_INTERNAL') || die();

use user_picture;
use context_course;

/**
 * Zegna ranking main utillity class
 *
 * @package   block_zegnaranking
 * @copyright 2017 Willian Mano http://conecti.me
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class zegnalib {
    /**
     * Return an array with the course modules and their sum points
     *
     * @param int $courseid
     * @param int $groupid
     *
     * @return array
     */
    public static function get_group_modules_points($courseid, $groupid) {
        global $DB, $OUTPUT;

        $sql = "SELECT
                	gi.id, gi.itemname, gi.itemmodule, SUM(gg.finalgrade) as points
                FROM {grade_grades} gg
                INNER JOIN {grade_items} gi ON gi.id = gg.itemid
                INNER JOIN {groups_members} gm ON gm.userid = gg.userid
                WHERE gi.itemtype = 'mod'
                AND gi.courseid = :courseid
                AND gm.groupid = :groupid
                GROUP BY gi.id
                ORDER BY gi.sortorder";

        $params['courseid'] = $courseid;
        $params['groupid'] = $groupid;

        $groupspoints = array_values($DB->get_records_sql($sql, $params));

        foreach ($groupspoints as $key => $group) {
            $groupspoints[$key]->points = (int) $group->points;
        }

        return $groupspoints;
    }

    /**
     * Return a general list of students and their points of the current module
     *
     * @param int $itemid The gradeitem id
     * @param int $courseid The course id
     * @param int $groupid The group id
     *
     * @return array
     */
    public static function get_group_module_users_points($itemid, $courseid, $groupid) {
        global $DB, $PAGE;

        $context = $PAGE->context;

        $userfields = user_picture::fields('u', array('username'));
        $sql = "SELECT
                    DISTINCT $userfields,
                    gg.userid,
                    gg.finalgrade as points
                FROM {grade_grades} gg
                INNER JOIN {user} u ON u.id = gg.userid
                INNER JOIN {groups_members} gm ON gm.userid = gg.userid
                INNER JOIN {grade_items} gi ON gi.id = gg.itemid
                INNER JOIN {role_assignments} ra ON ra.userid = gg.userid
                INNER JOIN {context} c ON c.id = ra.contextid
                WHERE
                    ra.contextid = :contextid
                    AND ra.userid = gg.userid
                    AND ra.roleid = :roleid
                    AND c.instanceid = :courseinstanceid
                    AND gi.id = :itemid
                    AND gm.groupid = :groupid
                ORDER BY points DESC, u.firstname ASC";
        $params['contextid'] = $context->id;
        $params['roleid'] = 5;
        $params['courseinstanceid'] = $courseid;
        $params['itemid'] = $itemid;
        $params['groupid'] = $groupid;

        $users = array_values($DB->get_records_sql($sql, $params));

        return $users;
    }

    /**
     * Return an array with the course groups
     *
     * @param int $courseid
     *
     * @return array
     */
    public static function get_course_groups($courseid) {
        global $DB;

        $groups = $DB->get_records('groups', ['courseid' => $courseid], null, 'id, name');

        $output[] = get_string('chooseagroup', 'block_zegnaranking');

        foreach ($groups as $group) {
            $output[$group->id] = $group->name;
        }

        return $output;
    }

    /**
     * Return an array with the course groups and their points
     *
     * @param int $courseid
     *
     * @return array
     */
    public static function get_course_groups_points($courseid) {
        global $DB;

        $sql = "SELECT
                	g.id, g.name, g.picture, g.hidepicture, SUM(gg.finalgrade) as points
                FROM {grade_grades} gg
                INNER JOIN {grade_items} gi ON gi.id = gg.itemid
                INNER JOIN {groups_members} gm ON gm.userid = gg.userid
                RIGHT JOIN {groups} g ON g.id = gm.groupid
                WHERE gi.itemtype = 'mod'
                AND gi.courseid = :courseid
                GROUP BY g.id
                ORDER BY points DESC";

        $params['courseid'] = $courseid;

        return $DB->get_records_sql($sql, $params);
    }
}
