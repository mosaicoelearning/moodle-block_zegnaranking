<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Zegna ranking block - map raking page
 *
 * @package    block_zegnaranking
 * @copyright  2017 Willian Mano http://conecti.me
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace block_zegnaranking\output;

defined('MOODLE_INTERNAL') || die();

use renderable;
use templatable;
use renderer_base;
use block_zegnaranking\zegnalib;

/**
 * Map ranking page renderable class.
 *
 * @package    block_zegnaranking
 * @copyright  2017 Willian Mano http://conecti.me
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mapranking_page implements renderable, templatable {

    /** @var array mappedgroups. */
    protected $mappedgroups;

    /** @var int courseid. */
    protected $courseid;

    /**
     * Constructor.
     *
     * @param int $courseid The course id
     *
     * @return void
     */
    public function __construct($courseid) {
        $this->courseid = $courseid;

        $this->mappedgroups = zegnalib::get_course_groups_points($courseid);
    }

    /**
     * Export the data.
     *
     * @param renderer_base $output
     *
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        global $CFG;

        $groups = $this->mappedgroups;

        $i = 1;
        $regions = [];
        foreach ($groups as $group) {
            $group->points = number_format($group->points, 2);
            $group->newline = false;

            $group->picture = print_group_picture($group, $this->courseid, true, true, false);

            if (!$group->picture) {
                $group->picture = "<img src='{$CFG->wwwroot}/blocks/zegnaranking/pix/default_group.png' alt='{$group->name}'>";
            }

            if (!($i % 4)) {
                $i = 0;
                $group->newline = true;
            }

            $i++;

            $regions[] = $group;
        }

        return ['regions' => $regions];
    }
}
