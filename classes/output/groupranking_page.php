<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Zegna ranking block - map raking page
 *
 * @package    block_zegnaranking
 * @copyright  2017 Willian Mano http://conecti.me
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace block_zegnaranking\output;

defined('MOODLE_INTERNAL') || die();

use renderable;
use templatable;
use renderer_base;
use block_zegnaranking\zegnalib;

/**
 * Group ranking page renderable class.
 *
 * @package    block_zegnaranking
 * @copyright  2017 Willian Mano http://conecti.me
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class groupranking_page implements renderable, templatable {

    /** @var array modules. */
    protected $modules;

    /** @var int the group. */
    protected $group;

    /**
     * Constructor.
     *
     * @param int $groupid The group id
     *
     * @return void
     */
    public function __construct($groupid) {
        global $DB;

        $this->group = $DB->get_record('groups', ['id' => $groupid], '*', 'MUST_EXISTS');
        $this->modules = zegnalib::get_group_modules_points($this->group->courseid, $this->group->id);
    }

    /**
     * Export the data.
     *
     * @param renderer_base $output
     *
     * @return stdClass
     */
    public function export_for_template(renderer_base $output) {
        return [
            'courseid' => $this->group->courseid,
            'groupid' => $this->group->id,
            'groupname' => $this->group->name,
            'modules' => $this->modules
        ];
    }
}
