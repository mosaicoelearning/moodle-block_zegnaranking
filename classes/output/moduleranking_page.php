<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Zegna ranking block - map raking page
 *
 * @package    block_zegnaranking
 * @copyright  2017 Willian Mano http://conecti.me
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace block_zegnaranking\output;

defined('MOODLE_INTERNAL') || die();

use renderable;
use templatable;
use renderer_base;
use block_zegnaranking\zegnalib;

/**
 * Module ranking page renderable class.
 *
 * @package    block_zegnaranking
 * @copyright  2017 Willian Mano http://conecti.me
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class moduleranking_page implements renderable, templatable {

    /** @var array Activity user and their points. */
    protected $users;

    /** @var array Group info. */
    protected $group;

    /** @var array Grade item info. */
    protected $gradeitem;

    /**
     * Constructor.
     *
     * @param $itemid The activity item id
     * @param $groupid The group id
     *
     * @return void
     */
    public function __construct($itemid, $groupid) {
        global $DB;

        $this->group = $DB->get_record('groups', ['id' => $groupid], '*', 'MUST_EXISTS');

        $this->gradeitem = $DB->get_record('grade_items', ['id' => $itemid], '*', 'MUST_EXISTS');

        $this->users = zegnalib::get_group_module_users_points($itemid, $this->group->courseid, $groupid);
    }

    /**
     * Export the data.
     *
     * @param renderer_base $output
     *
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        $data = $this->users;
        $outputdata = [];

        $lastpos = 1;
        $lastpoints = current($data)->points;
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i]->points == 0) {
                continue;
            }

            if ($lastpoints > $data[$i]->points) {
                $lastpos++;
                $lastpoints = $data[$i]->points;
            }

            $outputdata[] = [
                'pos' => $lastpos,
                'points' => (int) $data[$i]->points,
                'fullname' => $data[$i]->firstname . ' ' . $data[$i]->lastname,
                'userpicture' => $output->user_picture($data[$i], array('size' => 100, 'alttext' => false))
            ];
        }

        return [
            'itemname' => $this->gradeitem->itemname,
            'groupname' => $this->group->name,
            'students' => $outputdata
        ];
    }
}
