<?php
// This file is part of Ranking block for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Zegna ranking block - module raking page
 *
 * @package    block_zegnaranking
 * @copyright  2017 Willian Mano http://conecti.me
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__ . '/../../config.php');

$itemid = required_param('itemid', PARAM_INT);
$groupid = required_param('groupid', PARAM_INT);

$group = $DB->get_record('groups', ['id' => $groupid], '*', 'MUST_EXIST');

require_login($group->courseid);

$context = context_course::instance($group->courseid);

$url = new moodle_url('/blocks/zegnaranking/moduleranking.php', array('itemid' => $itemid, 'groupid' => $groupid));
$PAGE->set_context($context);
$PAGE->set_url($url);
$title = $group->name . ' - ' . get_string('moduleranking', 'block_zegnaranking');
$title .= ' - ' . get_string('individualclassification', 'block_zegnaranking');
$PAGE->set_title($title);
$PAGE->set_pagelayout('course');

// Add the page nav to breadcrumb.
$PAGE->navbar->add(
    get_string('map', 'block_zegnaranking'),
    new moodle_url('/blocks/zegnaranking/mapranking.php?courseid=' . $group->courseid)
);
$PAGE->navbar->add(
    get_string('group', 'block_zegnaranking'),
    new moodle_url('/blocks/zegnaranking/groupranking.php?groupid=' . $group->id)
);
$PAGE->navbar->add(get_string('module', 'block_zegnaranking'));

$output = $PAGE->get_renderer('block_zegnaranking');

echo $output->header();

echo $output->container_start('zegnaranking-modulereport');

$page = new \block_zegnaranking\output\moduleranking_page($itemid, $groupid);

echo $output->render($page);

echo $output->container_end();

echo $OUTPUT->footer();
