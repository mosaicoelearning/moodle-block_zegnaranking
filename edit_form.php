<?php
// This file is part of Ranking block for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Zegna ranking block configuration form definition
 *
 * @package   block_zegnaranking
 * @copyright 2017 Willian Mano http://conecti.me
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

use block_zegnaranking\zegnalib;
/**
 * Zegna ranking block configuration form definition class
 *
 * @copyright 2017 Willian Mano http://conecti.me
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_zegnaranking_edit_form extends block_edit_form {

    /**
     * Zegna ranking form definition
     *
     * @param mixed $mform
     * @return void
     */
    public function specific_definition($mform) {
        global $CFG, $PAGE;

        $mform->addElement('header', 'displayinfo', get_string('configuration', 'block_zegnaranking'));

        $mform->addElement('text', 'config_zegnaranking_title', get_string('blocktitle', 'block_zegnaranking'));
        $mform->setDefault('config_zegnaranking_title', get_string('ranking', 'block_zegnaranking'));
        $mform->addRule('config_zegnaranking_title', null, 'required', null, 'client');
        $mform->setType('config_zegnaranking_title', PARAM_RAW);

        $groups = zegnalib::get_course_groups($PAGE->course->id);

        // North america group settings.
        $mform->addElement(
            'select',
            'config_zegnaranking_region_northamerica',
            get_string('region_northamerica', 'block_zegnaranking'),
            $groups
        );
        $mform->addRule('config_zegnaranking_region_northamerica', null, 'required', null, 'client');
        $mform->setType('config_zegnaranking_region_northamerica', PARAM_INT);

        // Latam group settings.
        $mform->addElement(
            'select',
            'config_zegnaranking_region_latam',
            get_string('region_latam', 'block_zegnaranking'),
            $groups
        );
        $mform->addRule('config_zegnaranking_region_latam', null, 'required', null, 'client');
        $mform->setType('config_zegnaranking_region_latam', PARAM_INT);

        // Europe group settings.
        $mform->addElement(
            'select',
            'config_zegnaranking_region_europe',
            get_string('region_europe', 'block_zegnaranking'),
            $groups
        );
        $mform->addRule('config_zegnaranking_region_europe', null, 'required', null, 'client');
        $mform->setType('config_zegnaranking_region_europe', PARAM_INT);

        // Gcr group settings.
        $mform->addElement(
            'select',
            'config_zegnaranking_region_gcr',
            get_string('region_gcr', 'block_zegnaranking'),
            $groups
        );
        $mform->addRule('config_zegnaranking_region_gcr', null, 'required', null, 'client');
        $mform->setType('config_zegnaranking_region_gcr', PARAM_INT);

        // Asia group settings.
        $mform->addElement(
            'select',
            'config_zegnaranking_region_asia',
            get_string('region_asia', 'block_zegnaranking'),
            $groups
        );
        $mform->addRule('config_zegnaranking_region_asia', null, 'required', null, 'client');
        $mform->setType('config_zegnaranking_region_asia', PARAM_INT);

        // Supply chain group settings.
        $mform->addElement(
            'select',
            'config_zegnaranking_region_supply_chain',
            get_string('region_supply_chain', 'block_zegnaranking'),
            $groups
        );
        $mform->addRule('config_zegnaranking_region_supply_chain', null, 'required', null, 'client');
        $mform->setType('config_zegnaranking_region_supply_chain', PARAM_INT);
    }
}
