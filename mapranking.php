<?php
// This file is part of Ranking block for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Zegna ranking block - map raking page
 *
 * @package    block_zegnaranking
 * @copyright  2017 Willian Mano http://conecti.me
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__ . '/../../config.php');

$courseid = required_param('courseid', PARAM_INT);

$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);

require_login($courseid);

$context = context_course::instance($courseid);

$url = new moodle_url('/blocks/zegnaranking/mapranking.php', array('courseid' => $courseid));
$PAGE->set_context($context);
$PAGE->set_url($url);
$PAGE->set_title($course->fullname . ': ' . get_string('mapranking', 'block_zegnaranking'));
$PAGE->set_pagelayout('course');

// Add the page nav to breadcrumb.
$PAGE->navbar->add(get_string('map', 'block_zegnaranking'));

$output = $PAGE->get_renderer('block_zegnaranking');

echo $output->header();
echo $output->heading(get_string('mapranking', 'block_zegnaranking'));
echo $output->container_start('zegnaranking-mapreport');

$page = new \block_zegnaranking\output\mapranking_page($courseid);

echo $output->render($page);

echo $output->container_end();

echo $OUTPUT->footer();
